# Ansible role ``sys/net/interfaces.init``

> Unified network interface configuration for mixed environments.

## Overview
This Ansible role is part of the ``sys/net/interfaces`` role suite,
within which it handles initialization tasks. This role is invoked
by other roles from the ``sys/net/interfaces`` role suite to validate
role variables, and generate complex role facts for future consumption
by other roles.

The documentation of this role and its siblings is available in the
``sys/net/interfaces`` main role.
